<?php
require_once 'vendor/autoload.php';

use Src\Redis, Src\Cli\CliHelper;

if (isset($argv[1])) {
    $day = CliHelper::getDayInt($argv[1]);
    echo Redis::getInstance()->delete($day) . " records were remote";
} else {
    $day = readline("Insert number of days of relevance (records older than this period will be deleted): ");

    $day = CliHelper::getDayInt($day);

    $rows = CliHelper::getPreviewForCliByDay($day);

    if (!$rows) {
        echo 'No records!';
        echo PHP_EOL;
        exit();
    }

    foreach ($rows as $row) {
        echo PHP_EOL;
        echo $row;
    }
    echo PHP_EOL;
    echo '...';
    echo PHP_EOL;

    $confirm = readline("Are you sure you want to delete (Y/N): ");

    if (!CliHelper::getConfirmBool($confirm)) {
        echo 'Entries have not been deleted!';
        echo PHP_EOL;
        exit();
    }

    for ($i = rand(5, 10); $i > 0; --$i) {
        echo "\r";
        echo $i . ' ';
        sleep(1);
    }
    echo "\r";

    $count = Redis::getInstance()->getCountTimeKey((int) $day);

    CliHelper::showDeleteProcess((int) $day, 1000);

    print PHP_EOL;
    echo $count . " records were remote";
    echo PHP_EOL;
    exit();
}