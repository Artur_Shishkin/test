<?php

namespace Src\Controller;

use Src\Redis;
use Src\Rules\Custom;
use Src\Rules\In;
use Src\Rules\Json;
use Src\Rules\MaxLength;
use Src\Rules\Required;
use Src\Rules\RequiredWith;
use Src\Rules\Unique;
use Src\Validator;

class Controller extends AbstractApiController
{
    /**
     * @return string
     * @throws \Exception
     */
    public function viewAction(): string
    {
        $validator = new Validator($this->params);
        $validator->setRules([
            'key1' => [new Required(), new MaxLength(20), new RequiredWith(null, 'key2'), new In(['email','phone'])],
            'key2' => [new Required(), new MaxLength(32), new RequiredWith(null, 'key1'), new Custom(null, 'key1')]
        ]);

        if (!$validator->isValid()) {
            throw new \Exception(json_encode($validator->getError()));
        }

        $key = $this->params['key1'] . '__' . $this->params['key2'];
        return Redis::getInstance()->getConnection()->get($key) ?: 'EMPTY';
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function createAction(): string
    {
        $validator = new Validator($this->params);
        $validator->setRules([
            'key1' => [new Required(), new MaxLength(20), new In(['email','phone']), new Unique(null, 'key2')],
            'key2' => [new Required(), new MaxLength(32), new Custom(null, 'key1')],
            'value' => [new Required(), new Json()]
        ]);

        if (!$validator->isValid()) {
            throw new \Exception(json_encode($validator->getError()));
        }

        $key = $this->params['key1'] . '__' . $this->params['key2'];
        Redis::getInstance()->set($key, $this->params['value']);

        return 'OK';
    }
}