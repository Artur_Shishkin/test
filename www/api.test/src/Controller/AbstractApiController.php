<?php

namespace Src\Controller;

abstract class AbstractApiController
{
    /**
     * @var array
     */
    protected array $params = [];

    /**
     * @param array $params
     */
    public function setParams(array $params)
    {
        $this->params = $params;
    }

    /**
     * @return string
     */
    abstract public function viewAction(): string;

    /**
     * @return string
     */
    abstract public function createAction(): string;
}