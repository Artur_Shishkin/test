<?php

namespace Src\Cli;

use Src\Redis;

class CliHelper
{
    /**
     * @param string $day
     * @return array
     * @throws \Exception
     */
    public static function getPreviewForCliByDay(string $day): array
    {
        $keys = Redis::getInstance()
            ->getConnection()
            ->zRangeByScore('key', 0, strtotime('-' . $day . ' day'), ['limit' => [0, 5]]);

        $result = [];
        foreach ($keys as $key) {
            $result[] = 'key1=' . str_replace('__', ', key2=', $key) . ', value=' . Redis::getInstance()->getConnection()->get($key);
        }
        return $result;
    }

    /**
     * @param string $day
     * @return int
     * @throws \Exception
     */
    public static function getDayInt(string $day): int
    {
        if ($day !== (string) ((int) $day)) {
            throw new \Exception('Invalid day parameter');
        }

        return (int) $day;
    }

    /**
     * @param string $confirm
     * @return bool
     * @throws \Exception
     */
    public static function getConfirmBool(string $confirm): bool
    {
        if ('Y' !== $confirm && 'y' !== $confirm && 'N' !== $confirm && 'n' !== $confirm) {
            throw new \Exception('Invalid confirmation parameter (enter "Y" or "N")');
        }

        return ('y' === mb_strtolower($confirm)) ? true : false;
    }

    /**
     * @param int $percentage
     */
    public static function progressBar(int $percentage)
    {
        $percentageStringLength = 4;

        $percentageString = $percentage . '%';
        $percentageString = str_pad($percentageString, $percentageStringLength, " ", STR_PAD_LEFT);

        $percentageStringLength += 3;

        $barWidth = 100 - $percentageStringLength - 2;
        $numBars = round(($percentage) / 100 * ($barWidth));
        $numEmptyBars = $barWidth - $numBars;

        $barsString = '[' . str_repeat("=", ($numBars)) . str_repeat(" ", ($numEmptyBars)) . ']';

        echo "($percentageString) " . $barsString . "\r";
    }

    /**
     * @param int $day
     * @param int $batchSize
     */
    public static function showDeleteProcess(int $day, int $batchSize)
    {
        $count = Redis::getInstance()->getCountTimeKey($day);
        $total = ceil($count / $batchSize);
        for ($i = 1; $i <= $total; $i++) {
            Redis::getInstance()->deleteBatch($day, $batchSize);
            $percentage = $i / $total * 100;
            self::progressBar($percentage);
        }
        Redis::getInstance()->delTimeKeys($day);
    }
}