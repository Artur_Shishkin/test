<?php

namespace Src;

use Src\Controller\AbstractApiController;

class Handler
{
    /**
     * @var AbstractApiController
     */
    private AbstractApiController $controller;

    /**
     * Handler constructor.
     * @param AbstractApiController $controller
     */
    public function __construct(AbstractApiController $controller)
    {
        $this->controller = $controller;
        $this->controller->setParams($_REQUEST);
    }

    /**
     * @param int $status
     * @param string $data
     * @return string
     */
    private function getResponse(int $status, string $data = ''): string
    {
        header('Content-Type: application/json');
        header("HTTP/1.1 " . $status . " " . $this->requestStatus($status));
        return $data;
    }

    /**
     * @param int $code
     * @return string
     */
    private function requestStatus(int $code): string
    {
        $status = [
            200 => 'OK',
            400 => 'Bad Request',
            405 => 'Method Not Allowed',
            500 => 'Internal Server Error',
        ];
        return $status[$code] ?? $status[500];
    }

    /**
     * @return string|null
     */
    private function getAction(): ?string
    {
        switch ($_SERVER['REQUEST_METHOD']) {
            case 'GET':
                return 'viewAction';
                break;
            case 'POST':
                return 'createAction';
                break;
            default:
                return null;
        }
    }

    /**
     * @return string
     */
    public function run(): string
    {
        $action = $this->getAction();

        if (method_exists($this->controller, $action)) {
            try {
                $data = $this->controller->$action();
                return $this->getResponse(200, $data);
            } catch (\Exception $e) {
                return $this->getResponse(400, $e->getMessage());
            }
        } else {
            return $this->getResponse(405);
        }
    }
}