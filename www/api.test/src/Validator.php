<?php

namespace Src;

use Src\Rules\Rule;

class Validator
{
    private array $data = [];
    private array $errorList = [];

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function setRules(array $rules)
    {
        foreach ($rules as $attributeKey => $ruleClassList) {
            if (!is_array($ruleClassList)) {
                $ruleClassList = [$ruleClassList];
            }
            foreach ($ruleClassList as $ruleClass) {
                $this->validation($attributeKey, $ruleClass);
            }
        }
    }

    private function validation(string $attributeKey, Rule $rule): bool
    {
        $attribute = $this->data[$attributeKey] ?? '';

        if ($rule->getAddictedFieldKey()) {
            $addictedFieldValue = $this->data[$rule->getAddictedFieldKey()] ?? null;
            $rule->setAddictedFieldValue($addictedFieldValue);
        }

        if ($rule->check($attribute)) {
            return true;
        } else {
            $this->errorList[] = str_replace(':attribute', $attributeKey, $rule->getMessage());
            return false;
        }
    }

    public function isValid(): bool
    {
        return empty($this->errorList);
    }

    public function getError(): array
    {
        return $this->errorList;
    }
}