<?php

namespace Src;

class Redis
{
    private static Redis $instance;

    private \Redis $redis;

    /**
     * Redis constructor.
     */
    private function __construct() {
        $this->redis = new \Redis();
        $this->redis->connect('redis');
    }
    private function __clone() { }
    private function __wakeup() { }

    /**
     * @return Redis
     */
    public static function getInstance(): Redis
    {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @return \Redis
     */
    public function getConnection(): \Redis
    {
        return $this->redis;
    }

    /**
     * @param string $key
     * @param string $value
     */
    public function set(string $key, string $value)
    {
        $this->redis->zAdd('key', time(), $key);
        $this->redis->set($key, $value);
//        $this->redis->set($key . '_user', $_SERVER['PHP_AUTH_USER']);
    }

    /**
     * @param int $day
     * @return int
     */
    public function delete(int $day): int
    {
        $keys = $this->redis->zRangeByScore('key', 0, strtotime('-' . $day . ' day'));
        $this->delTimeKeys($day);
        return $this->redis->del($keys);
    }

    /**
     * @param int $day
     * @param int $batchSize
     * @return int
     */
    public function deleteBatch(int $day, int $batchSize = 10000): int
    {
        $keys = $this->redis->zRangeByScore('key', 0, strtotime('-' . $day . ' day'), ['limit' => [0, $batchSize]]);
        return $this->redis->del($keys);
    }

    /**
     * @param int $day
     * @return int
     */
    public function delTimeKeys(int $day): int
    {
        return $this->redis->zRemRangeByScore('key', 0, strtotime('-' . $day . ' day'));
    }

    /**
     * @param int $day
     * @return int
     */
    public function getCountTimeKey(int $day): int
    {
        return $this->redis->zCount('key', 0, strtotime('-' . $day . ' day'));
    }
}