<?php

namespace Src\Rules;

class MaxLength extends Rule
{
    /**
     * @return string
     */
    public function getMessage(): string
    {
        return 'The :attribute max length ' . $this->params;
    }

    /**
     * @param $value
     * @return bool
     */
    public function check($value): bool
    {
        if ($value === '') {
            return true;
        }

        return strlen($value) <= $this->params;
    }
}