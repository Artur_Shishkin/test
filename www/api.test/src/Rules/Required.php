<?php

namespace Src\Rules;

class Required extends Rule
{
    /**
     * @return string
     */
    public function getMessage(): string
    {
        return 'The :attribute is required';
    }

    /**
     * @param $value
     * @return bool
     */
    public function check($value): bool
    {
        return (bool)$value;
    }
}