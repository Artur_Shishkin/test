<?php

namespace Src\Rules;

class Email extends Rule
{
    /**
     * @return string
     */
    public function getMessage(): string
    {
        return 'The :attribute is not valid email';
    }

    /**
     * @param $value
     * @return bool
     */
    public function check($value): bool
    {
        if ($value === '') {
            return true;
        }
        return filter_var($value, FILTER_VALIDATE_EMAIL) !== false;
    }
}