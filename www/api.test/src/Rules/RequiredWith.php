<?php

namespace Src\Rules;

class RequiredWith extends Rule
{
    /**
     * @return string
     */
    public function getMessage(): string
    {
        return 'The :attribute dependent on ' . $this->addictedFieldKey;
    }

    /**
     * @param $value
     * @return bool
     */
    public function check($value): bool
    {
        if ($value === '') {
            return true;
        }
        return isset($this->addictedFieldValue);
    }
}