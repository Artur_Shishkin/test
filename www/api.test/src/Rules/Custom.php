<?php

namespace Src\Rules;

class Custom extends Rule
{
    /**
     * @return string
     */
    public function getMessage(): string
    {
        return 'The :attribute must be an email or phone number like 0950000000';
    }

    /**
     * @param $value
     * @return bool
     */
    public function check($value): bool
    {
        if ($value === '') {
            return true;
        }

        if ($this->addictedFieldValue == 'phone') {
            return filter_var($value, FILTER_VALIDATE_REGEXP, ["options" => ["regexp"=>"/^0[0-9]{9}$/"]]);
        }

        if ($this->addictedFieldValue == 'email') {
            return filter_var($value, FILTER_VALIDATE_EMAIL) !== false;
        }

        return false;
    }
}