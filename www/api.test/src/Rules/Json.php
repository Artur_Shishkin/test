<?php

namespace Src\Rules;

class Json extends Rule
{
    /**
     * @return string
     */
    public function getMessage(): string
    {
        return 'The :attribute is not valid json';
    }

    /**
     * @param $value
     * @return bool
     */
    public function check($value): bool
    {
        if ($value === '') {
            return true;
        }

        return null === json_decode($value);
    }
}