<?php

namespace Src\Rules;

use Src\Redis;

class Unique extends Rule
{
    /**
     * @return string
     */
    public function getMessage(): string
    {
        return 'Such record already exists in the database';
    }

    /**
     * @param $value
     * @return bool
     */
    public function check($value): bool
    {
        if ($value === '') {
            return true;
        }

        return !Redis::getInstance()->getConnection()->exists($value . '__' . $this->addictedFieldValue);
    }
}