<?php

namespace Src\Rules;

abstract class Rule
{
    /**
     * @var string|null
     */
    protected ?string $addictedFieldKey;

    /**
     * @var mixed
     */
    protected $addictedFieldValue;

    /**
     * @var mixed
     */
    protected $params;

    /**
     * Rule constructor.
     * @param string|null $addictedFieldKey
     * @param mixed|null $params
     */
    public function __construct($params = null, ?string $addictedFieldKey = null)
    {
        $this->addictedFieldKey = $addictedFieldKey;
        $this->params = $params;
    }

    /**
     * @return string|null
     */
    public function getAddictedFieldKey(): ?string
    {
        return $this->addictedFieldKey;
    }

    /**
     * @param $value
     */
    public function setAddictedFieldValue($value)
    {
        $this->addictedFieldValue = $value;
    }

    /**
     * @param $value
     * @return bool
     */
    abstract public function check($value): bool;

    /**
     * @return string
     */
    abstract public function getMessage(): string;
}