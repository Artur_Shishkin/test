<?php

namespace Src\Rules;

class In extends Rule
{
    /**
     * @return string
     */
    public function getMessage(): string
    {
        return 'The :attribute must be off the list ("email", "phone")';
    }

    /**
     * @param $value
     * @return bool
     */
    public function check($value): bool
    {
        if ($value === '') {
            return true;
        }

        return in_array($value, $this->params);
    }
}