<?php
require_once 'vendor/autoload.php';

use Src\Handler, Src\Controller\Controller;

$controller = new Controller();
$handler = new Handler($controller);
echo $handler->run();
